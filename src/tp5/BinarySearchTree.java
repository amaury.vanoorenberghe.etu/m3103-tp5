package tp5;

import java.util.Comparator;
import java.util.function.Function;

@SuppressWarnings("unused")
public class BinarySearchTree<K, V> {
	private final Comparator<K> KEY_COMP;
	private Pair<K, V> item;
	private BinarySearchTree<K, V> left, right;
	
	public BinarySearchTree() {
		this((K)null, (V)null);
	}
	
	public BinarySearchTree(K key, V value) {
		this(key, value, null);
	}
	
	public BinarySearchTree(K key, V value, Comparator<K> keyComp) {
		this(key, value, keyComp, null, null);
		left = new BinarySearchTree<K, V>(null, null, keyComp, null, null);
		right = new BinarySearchTree<K, V>(null, null, keyComp, null, null);
	}
	
	public BinarySearchTree(K key, V value, BinarySearchTree<K, V> left, BinarySearchTree<K, V> right) {
		this(key, value, null, left, right);
	}
	
	private BinarySearchTree(BinarySearchTree<K, V> left, BinarySearchTree<K, V> right) {
		this(null, null, null, left, right);
	}
	
	private BinarySearchTree(Comparator<K> keyComp, BinarySearchTree<K, V> left, BinarySearchTree<K, V> right) {
		this(null, null, keyComp, left, right);
	}
	
	public BinarySearchTree(K key, V value, Comparator<K> keyComp, BinarySearchTree<K, V> left, BinarySearchTree<K, V> right) {
		this.item = new Pair<K, V>(key, value);
		this.KEY_COMP = keyComp;
		this.left = left;
		this.right = right;
	}
	
	public V put(K key, V value) {
		if (isEmpty()) {
			copy(new BinarySearchTree<K, V>(key, value, KEY_COMP));
			return null;
		}
		
		if (keyValueEquals(key, value)) {
			return value;
		}
		
		final BinarySearchTree<K, V> keyBST = search(key);
		if (!keyBST.isEmpty()) {
			final V oldValue = keyBST.getValue();
			keyBST.item.setValue(value);
			return oldValue;
		}
		
		return compare(this.getKey(), key) > 0
			 ? left.put(key, value)
			 : right.put(key, value);
	}
	
	public V get(K key) {
		return search(key).getValue();
	}
	
	public V remove(K key) {
		BinarySearchTree<K, V> keyBST = search(key);
		
		if (keyBST.isEmpty()) {
			return null;
		}
		
		final V oldValue = keyBST.getValue();
		final BinarySearchTree<K, V> replacementBST = left.bstMax();//right.bstMin();
		
		keyBST.item = replacementBST.item;
		replacementBST.copy(replacementBST.left);
		
		return oldValue;
	}
	
	public void clear() {
		copy(new BinarySearchTree<K, V>((K)null, (V)null, KEY_COMP));
	}
	
	private BinarySearchTree<K, V> search(K key) {
		if (isEmpty()) {
			return this;
		}
		
		if (key.equals(this.getKey())) {
			return this;
		}
		
		final int comp = compare(this.getKey(), key);
		if (comp > 0) {
			return right.search(key);
		} else if (comp < 0) {
			return left.search(key);
		}
		
		return this;
	}
	
	protected BinarySearchTree<K, V> bstMin() {
		return bstFind(bst -> bst.left);
	}
	
	protected BinarySearchTree<K, V> bstMax() {
		return bstFind(bst -> bst.right);
	}
	
	private BinarySearchTree<K, V> bstFind(Function<BinarySearchTree<K, V>, BinarySearchTree<K, V>> next) {
		BinarySearchTree<K, V> value = this;
		BinarySearchTree<K, V> tmp;
		
		while ((tmp = next.apply(value)) != null && !tmp.isEmpty()) {
			value = tmp;
		}
		
		return value;
	}
	
	public V min() {
		return bstMin().getValue();
	}
	
	public V max() {
		return bstMax().getValue();
	}
	
	protected boolean isEmpty() {
		return item == null || getKey() == null;
	}
	
	public K getKey() {
		return item.getKey();
	}
	
	public V getValue() {
		return item.getValue();
	}
	
	protected void copy(BinarySearchTree<K, V> other) {
		item.setKey(other.getKey());
		item.setValue(other.getValue());
		left = other.left;
		right = other.right;
	}
	
	@SuppressWarnings("unchecked")
	protected int compare(K a, K b) {
		if (KEY_COMP != null) {
			return KEY_COMP.compare(a, b);
		}
		return ((Comparable<K>)a).compareTo(b);
	}
	
	private boolean keyValueEquals(K key, V value) {
		return !isEmpty()
			&& this.getKey().equals(key)
			&& this.getValue().equals(value);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		if (!left.isEmpty()) {
			sb.append(String.format("%s < ", left));
		}
		
		sb.append(String.format("%s = %s", getKey(), getValue()));
		
		if (!right.isEmpty()) {
			sb.append(String.format(" > %s", right));
		}
		
		return String.format("(%s)", sb);
	}
}
